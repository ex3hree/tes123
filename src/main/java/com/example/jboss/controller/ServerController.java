package com.example.jboss.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServerController {
    @Autowired
    private Environment env;

    @GetMapping("/")
    public String health() {
        return "{healthy:true}";
    }

    @GetMapping("/port")
    public String getPort() {
        return "Port number: " + env.getProperty("local.server.port");
    }




}
